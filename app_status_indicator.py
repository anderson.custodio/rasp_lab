import RPi.GPIO as GPIO
import time
import requests

import signal
import sys

import json

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

GPIO.setup(18, GPIO.OUT)
GPIO.setup(15, GPIO.OUT)
GPIO.setup(14, GPIO.OUT)
GPIO.setup(23, GPIO.OUT)

GPIO.output(18, GPIO.LOW)
GPIO.output(15, GPIO.LOW)
GPIO.output(14, GPIO.LOW)
GPIO.output(23, GPIO.LOW)


def sig_handler(sig, frame):
    off(18)
    off(15)
    off(14)
    off(23)
    sys.exit(0)


signal.signal(signal.SIGINT, sig_handler)


def verify_status(url, pin):
    try:
        req = requests.get(url)
        if req.status_code == 200:
            on(pin)
        else:
            off(pin)
    except requests.exceptions.ConnectionError:
        off(pin)


def verify_pipeline(url, pin):
    apikey = str(sys.argv[1])
    headers = {'PRIVATE-TOKEN':apikey}
    req = requests.get(url, headers=headers)
    result = req.json()[0]
    print(result)

    status = result.get('status')
    print(status)

    if status == 'success':
        print('success')
        on(pin)
    elif status == 'running':
        print('running')
        on(pin)
        time.sleep(1)
        off(pin)
        time.sleep(1)
        on(pin)
        time.sleep(1)
        off(pin)
        time.sleep(1)
        on(pin)
    else:
        print('else')
        off(pin)

def on(pin):
    print('UP', pin)
    GPIO.output(pin, GPIO.HIGH)


def off(pin):
    print('DOWN', pin)
    GPIO.output(pin, GPIO.LOW)

while True:
    print('Verificando...')
    verify_status('http://gd-legal-api-tests.gabinete.io/api/actuator/health', 18)
    verify_status('http://gd-provider-lfs-api-tests.gabinete.io/actuator/health', 15)
    verify_status('http://gd-ai-api-tests.gabinete.io/actuator/health', 14)
    verify_pipeline('https://gitlab.com/api/v4/projects/7279848/pipelines', 23)
    time.sleep(1)
